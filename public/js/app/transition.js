var Transition = function(){  
        i=0;   
     page=1;
     order_value='asc';
     var users =[];   
     var filter_name='';  
    init = function(){
        $("#add_transition").click(function(){
            save_transition();
        });  
        get_transition(page);
        $(".pagination").delegate('#paginate_a','click',function(){
            pagination_func(this);
        });
        $("#link").click(function(){
            order(this);
        });
        $("#link1").click(function(){
            order(this);
        });
        $("#add_btn").click(function(){
            clear_data();
        });
        $("#pagi_no").change(function(){
            get_transition(1);
        })
        // setTimeout(function(){
        // $('.auto').select2({
        //     data: autocomplete_func()
        // }); },2000);
        // $('.auto').change(function(){
        //     filter();
        // });
        $("#transition_body").delegate('#edit_btn','click',function(){
            Edit(this);
        });
        $("#transition_body").delegate('#delete_btn','click',function(){
            Delete(this);
        });
    },
    save_transition=function(){
        var add_user_id = $("#add_user_id").val();
        var status = $("#status").val();
        var reason = $("#reason").val();
        var amount = $("#amount").val();
        var category_id = $("#category_id").val();
        var id=$("#transition_id").val();
        if(id==''){
            $.ajax({
                type : 'post',
                url : Config.apiurl+"transition",
                dataType : 'json',
                data : {
                    user_id : add_user_id,
                    status : status,
                    reason : reason,
                    amount : amount,
                    category_id : category_id
                },
                success:function(data){
                    if(data==1){
                        alert("Successfully Saved!");
                        get_transition(1);
                    }
                    else{
                        alert("Please Try Again!");
                    }           
                },
            });      
        }
        else{
            $.ajax({
                type : 'put',
                url : Config.apiurl+"transition/"+id,
                dataType : 'json',
                data : {
                    id : id,
                    user_id : add_user_id,
                    status : status,
                    reason : reason,
                    amount : amount,
                    category_id : category_id
                },
                success:function(data){
                    if(data==1){
                        alert("Successfully Updated!");
                        get_transition(1);
                    }
                    else{
                        alert("Please Try Again!");
                    }           
                },
            });      
        }
    }
    get_transition= function(page){
        var id=$("#add_user_id").val();
        var pagi_num=$("#pagi_no").val();
        $.ajax({
            type : 'get',
            url : Config.apiurl+"transition?page="+page+"&order="+order_value+"&pagi_num="+pagi_num+"&name="+id,
            dataType : 'json',
            success : function(result){
             $("#transition_body").empty();
             $("#pagi_li").empty();
             paginate=result.data;             
             output=result.data.data;         
             current=result.current_amount;
             console.log(current);
             if(order_value=='asc'){
                if(page==1){
                    i=page-1;
                }
                else{
                    i=(page*pagi_num)-pagi_num;
                }                       
                output.forEach(function(e){
                    i++;                   
                    if(e['status']==1){
                        var sign='-';
                        var Inmoney='';
                        var Outmoney=e['amount'];
                    }
                    else{
                        var sign='';
                        var Inmoney=e['amount'];
                        var Outmoney='';
                    }                 
                    $('#transition_body').append("<tr><td>"+e['name']+"</td><td>"+e['category_name']+"</td><td>"+Inmoney+"</td><td>"+sign+Outmoney+"</td><td>"+current[i]+"</td><td>"+e['reason']+"</td><td>"+e['created_at']+"</td>"+
                    "<td><button class='btn btn-success' id='edit_btn' edit_id='"+e['id']+"'>Edit</button></td>"+
                    "<td><button class='btn btn-danger' id='delete_btn' delete_id='"+e['id']+"'>DELETE</button></td></tr>");
                });
            }
            else{
                current_length=current.length;
                 if(page==1){
                    i=current_length;
                }
                else{
                    i=current_length-(pagi_num*(page-1));
                }                       
                output.forEach(function(e){
                    i--;                   
                    if(e['status']==1){
                        var sign='-';
                        var Inmoney='';
                        var Outmoney=e['amount'];
                    }
                    else{
                        var sign='';
                        var Inmoney=e['amount'];
                        var Outmoney='';
                    }                 
                    $('#transition_body').append("<tr><td>"+e['name']+"</td><td>"+e['category_name']+"</td><td>"+Inmoney+"</td><td>"+sign+Outmoney+"</td><td>"+current[i]+"</td><td>"+e['reason']+"</td><td>"+e['created_at']+"</td>"+
                    "<td><button class='btn btn-success' id='edit_btn' edit_id='"+e['id']+"'>Edit</button></td>"+
                    "<td><button class='btn btn-danger' id='delete_btn' delete_id='"+e['id']+"'>DELETE</button></td></tr>");
                });

            }
             
                for($i=1;$i<=paginate.last_page;$i++)
                {   
                    $("#pagi_li").append("<a href='#' id='paginate_a' page_no='"+$i+"'>"+$i+"</a>");
                }   
                        
            }
        });
    }
    pagination_func=function(e){         
        page=$(e).attr("page_no");
        get_transition(page);
    }
    clear_data=function(){
        $("#status").val('');
        $("#category_id").val('');
        $("#reason").val('');
        $('#amount').val('');
    }
    // filter=function(){
    //   filter_name =$(".auto").val();
    //   // get_transition(1);
    // }
    Edit=function(obj){
      id=$(obj).attr("edit_id");
      $.ajax({
            type: 'get',
            url : Config.apiurl+"transition/"+id+"/edit",
            dataType : 'json',
            success : function(result){
                output=result;
                $("#myModal").modal('show');  
                $("#transition_id").val(output[0].id);              
                $("#amount").val(output[0].amount);
                $("#status").val(output[0].status);
                $("#reason").text(output[0].reason);
                $("#category_id option[value='"+output[0].category_id+"']").attr("selected", "selected");                                               
            } 
        });
    }
    Delete=function(obj){
        id=$(obj).attr("delete_id");
        $.ajax({
            type: 'delete',
            url : Config.apiurl+"transition/"+id,
            dataType : 'json',
            success : function(result){
                if(result==1){
                    alert("Successfully Deleted!");
                    get_transition(1);
                }
                else{
                    alert("Please try again");
                }                
            } 
        });
    }
    selected=function(){
        return "selected";
    }
    // autocomplete_func=function(){        
    //     var for_loop=Category.category;
    //         for_loop.forEach(function(e){
    //             var data={
    //                 'id' : e.id,
    //                 'text': e.category_name
    //             };
    //             users.push(data);
    //         });  
    //         return users;                     
    //}
    order=function(obj){
        order_value=$(obj).attr('order');
        get_transition(1);
    }
    return{
        init : init,
    }
}();

$(document).ready(function(){ 
    Transition.init();
    var all_category = Category.category;   
    setTimeout(function(){   
        all_category.forEach(function(c){
             $('#category_id').append("<option value='"+c['id']+"'>"+c['category_name']+"</option>");
        });
    }, 2000);
});