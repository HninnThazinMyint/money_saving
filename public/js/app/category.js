var Category = function(){
	 // global variable to use in other file
    page=1;
    category=[];
    init = function(){        
	 	$("#add_category").click(function(){
	 		saveCategory();
	 	});  
        $("#add_btn").click(function(){
            cleardata();
        });
        getCategory(page);

        $(".pagination").delegate('#paginate_a','click',function(){
            pagination_func(this);
        });
        $("#category_body").delegate('#edit_btn','click',function(){
            Edit(this);
        });
        $("#category_body").delegate('#delete_btn','click',function(){
            Delete(this);
        });
        getall_Cat();
    },
    saveCategory = function(){
	    var cat_name=$('#category_name').val();
        var cat_description=$('#category_description').val();
        var id=$('#cat_id').val();
        if(id==''){
            $.ajax({
                type : 'post',
                url : Config.apiurl+"category",
                dataType : 'json',
                data : {
                    name : cat_name,
                    description : cat_description
                },
                success:function(data){
                    if(data==1){
                        alert("Successfully Saved!");
                        getCategory(1);
                    }
                    else{
                        alert("Please Try Again!");
                    }           
                },
                error :function(error){                    
                    if(error!='undefined' && error!=''){
                        $("#myModal").modal('show');
                        var common_err=error.responseJSON.errors;                        
                        if(common_err.name!='undefined' ){
                            $("#name_err").text(common_err.name).css('color','red');
                        }
                        if(common_err.description!='undefined' ){
                            $("#description_err").text(common_err.description).css('color','red');
                        }                                         
                    }
                    return false;
                }
            });
        }
        else{
             $.ajax({
                type : 'put',
                url : Config.apiurl+"category/"+id,
                dataType : 'json',
                data :{
                    id : id,
                    name : cat_name,
                    description : cat_description
                },
                success : function(data){
                    if(data==1){
                        alert("Successfully updated!");
                        getCategory(1);
                    }
                    else{
                        alert("Please Try Again!");
                    }      
                }
            });
        }
 		
    }
    getCategory=function(page){
        $.ajax({
            type: 'get',
            url : Config.apiurl+"category?page="+page,
            dataType : 'json',
            success : function(result){
                $("#category_body").empty();
                $("#pagi_li").empty();
               paginate=result.data;
               output=result.data.data;
               output.forEach(function(e){
                $("#category_body").append("<tr><td>"+e['category_name']+"</td><td>"+e['description']+"</td><td><button class='btn btn-primary' id='edit_btn' edit_id='"+e['id']+"'>Edit</button></td><td><button class='btn btn-danger' id='delete_btn' delete_id='"+e['id']+"'>DELETE</button></td></tr>");
                });   
               for($i=1;$i<=paginate.last_page;$i++)
               {   
                  $("#pagi_li").append("<a href='#' id='paginate_a' page_no='"+$i+"'>"+$i+"</a>");
               }
            }
        });
    }
    pagination_func=function(e){         
        page=$(e).attr("page_no");
        getCategory(page);
    }
    Edit=function(obj){
      id=$(obj).attr("edit_id");
      $.ajax({
            type: 'get',
            url : Config.apiurl+"category/"+id+"/edit",
            dataType : 'json',
            success : function(result){
                output=result;
                $("#myModal").modal('show');
                $("#cat_id").val(output.id);
                $("#category_name").val(output.category_name);
                $("#category_description").val(output.description);
            } 
        });
    }
    cleardata=function(){
        $("#category_name").val('');
        $("#category_description").val('');
        $("#name_err").text('');
        $("#description_err").text('');

    }
    Delete=function(obj){
        id=$(obj).attr("delete_id");
        $.ajax({
            type: 'delete',
            url : Config.apiurl+"category/"+id,
            dataType : 'json',
            success : function(result){
                if(result==1){
                    alert("Successfully Deleted!");
                    getCategory(1);
                }
                else{
                    alert("Please try again");
                }                
            } 
        });
    }
    getall_Cat=function(){
        $.ajax({
            type: 'get',
            url : Config.apiurl+"getcategory",
            dataType : 'json',
            success : function(result){
               result.forEach(function(e){
                    category.push(e);
                });
            } 
        });
    }
    return{
        init : init,
        category : category
    }

}();

$(document).ready(function(){
    Category.init();
});