var User = function(){
	category = "chosen", 
    user=[],
    page=1;// global variable to use in other file
    init = function(){        
	 	$("#add_user").click(function(){
	 		saveUser();
	 	}); 
	 	getUser(page); 
        getAll();
        $(".pagination").delegate('#paginate_a','click',function(){
            pagination_func(this);
        });
    },
    saveUser = function(){
	    var name=$("#name").val(); 		
 		var email=$("#email").val();
 		var password=$("#password").val();
 		var phone=$("#phone").val();
 		var address=$("#address").val();
 		$.ajax({
 			type : 'post',
 			url : Config.apiurl+"user",
 			dataType : 'json',
 			data : {
 				name : name,
 				email : email,
 				password : password,
 				phone : phone,
 				address : address,
 			},
 			success:function(data){
	 			if(data==1){
	 				alert("Successfully Saved!");
	 				location.reload();
	 			}
	 			else{
	 				alert("Please Try Again!");
	 			} 			
 			}
  		});
    }
    getUser=function(page){
    	$.ajax({
    		type: 'get',
    		url : Config.apiurl+"user?page="+page,
    		dataType : 'json',
    		success : function(result){
                $("#show_output").html('');
                $("#pagi_li").html('');
               paginate=result.data;
    		   output=result.data.data;
    		   output.forEach(function(e){
                $("#show_output").append("<tr><td>"+e['name']+"</td><td>"+e['phone']+"</td><td>"+e['address']+"</td><td>"+e['email']+"</tr>");
                });	  
               for($i=1;$i<=paginate.last_page;$i++)
               {   
                  $("#pagi_li").append("<a href='#' id='paginate_a' page_no='"+$i+"'>"+$i+"</a>");
               }
    		}
    	});
    }
    pagination_func=function(e){         
        page=$(e).attr("page_no");
        getUser(page);
    }
    getAll=function(){
        $.ajax({
            type: 'get',
            url : Config.apiurl+"getuser",
            dataType : 'json',
            success : function(result){
              result.forEach(function(e){
                user.push(e);
              });// var user="hello";
            }
        });
    }
    return{
        init : init,
        user: user,
    }

}();

$(document).ready(function(){
    User.init();
});