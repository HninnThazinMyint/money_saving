<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {   
});
Route::get('/getcategory','CategoryController@getAll_Cat');
Route::resource('category','CategoryController');
Route::post('/login_user','UserController@login');
Route::resource('user','UserController');
Route::get('/getuser','UserController@getall');
Route::resource('transition','TransitionController');


