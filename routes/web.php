<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login and logout
Route::post('login_user','UserController@login');
Route::get('logout','UserController@logout');

Route::get('/', function () {
    return view('User.login');
});
Route::group(['middleware' => ['checkroute']], function () {
    Route::get('/user',function(){
	 return view('User.insert');
  });
	Route::get('/category',function(){
		return view('Category.category_insert');
	});
	Route::get('/transition',function(){
		return view('Transition.transition');
	});

	Route::get('/home',function(){
		return view('User.home_page');
	});
});

Route::get('/user', function () {
	 return view('User.insert');
})->middleware('checkadmin');
  