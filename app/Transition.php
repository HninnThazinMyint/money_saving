<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transition extends Model
{
    protected $fillable=['status','amount','reason','category_id','user_id'];
 	protected $table = 'transitions';
}
