<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class checkadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(Session::get('user')==''){
            return redirect('/');
        }    
        if(Session::get('user')!='' && Session::get('user')->role != 1){
            return redirect('/home');
        }     
        return $next($request);
    }
}
