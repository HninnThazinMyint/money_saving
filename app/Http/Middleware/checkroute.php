<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Session;

class checkroute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(Session::get('user') == ''){
            return redirect('/');
        }     
        return $next($request);
    }
}
