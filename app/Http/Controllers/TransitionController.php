<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Transition;
use App\Category;
use App\User;
class TransitionController extends Controller
{   
    public function __construct()
    {
        $tt = new Transition();      
        $this->data = $tt->leftJoin('users', 'users.id', '=', 'transitions.user_id')
                            ->leftJoin('categories', 'categories.id', '=', 'transitions.category_id')
                            ->select('users.name','users.id AS user_id','categories.id AS category_id','categories.category_name', 'transitions.status','transitions.reason','transitions.id','transitions.amount','transitions.created_at');          
    }
    public function index(Request $request)
    {   
        $current_amount=array();
        $current_amount[0]=0;
        $rr= $request->all();        
        $name= $rr['name'];
        $transition = $this->data;                
        if($name!="" && $name!=null){
           $transition=$transition->where('users.id', '=', $name);
        }
        $current=$transition->get();
        foreach($current as $c){
            if($c['status']==0){
                $money=end($current_amount);
                $money= $money+$c['amount'];
            }
            else{
                $money=end($current_amount);
                $money=$money-$c['amount'];
            }
            array_push($current_amount, $money);
        }
        $transition=$transition->orderBy('transitions.created_at',$rr['order'])->paginate($rr['pagi_num']);
        $response = [
            'pagination' => [
                'total' => $transition->total(),
                'per_page' => $transition->perPage(),
                'current_page' => $transition->currentPage(),
                'last_page' => $transition->lastPage(),
                'from' => $transition->firstItem(),
                'to' => $transition->lastItem()
            ],
             'data' => $transition,
             'current_amount' => $current_amount
        ];
        return response()->json($response);
    }
  
    public function store(Request $request)
    {   
        $transition = new Transition([
            'status' => $request->get('status'),
            'amount' => $request->get('amount'),
            'reason' => $request->get('reason'),
            'category_id' => $request->get('category_id'),
            'user_id' => $request->get('user_id'),
        ]);
        $result = $transition->save();
        echo json_encode($result);
    }
    public function edit($id)
    {    
       $transition = $transition = $this->data; 
       $edit_val = $transition->where('transitions.id','=',$id)->get();
       return response()->json($edit_val);
    }

    
    public function update(Request $request, $id)
    {
        $post=[
            'status' =>$request->get('status'),
            'amount'   => $request->get('amount'),
            'reason' =>$request->get('reason'),
            'category_id'   => $request->get('category_id'),
            'user_id'   => $request->get('user_id'),
        ];
        $result = Transition::where('id',$id)->update($post);
        echo json_encode($result);
    }    
    public function destroy($id)
    {
       $transition = Transition::find($id);
       $result = $transition->delete();
       echo json_encode($result);       
    }
}
