<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;
use JWTAuth;
use JWTAuthException;
class UserController extends Controller
{   
    public function index()
    {
        $user= new User();
        $user=$user->paginate(4);
        $response = [
            'pagination' => [
                'total' => $user->total(),
                'per_page' => $user->perPage(),
                'current_page' => $user->currentPage(),
                'last_page' => $user->lastPage(),
                'from' => $user->firstItem(),
                'to' => $user->lastItem()
            ],
             'data' => $user
        ];
        return response()->json($response);
    }
    public function getall(){
        $user= new User();
        $user=$user->all();
        return response()->json($user);
    }

    public function store(Request $request)
    {   
        $user=new User([
            'name' => $request->get('name'),
            'role' => 0,
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'address' => $request->get('address')
        ]);
        $result=$user->save();
        return json_encode($result);
    }
    public function update(Request $request, $id)
    {
        $post=([
            'name' => $request->get('name'),
            'role' =>0,
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'address' => $request->get('address')
        ]);
        User::where('id',$id)->update($post);
        return json_encode("Successfully updated!");
    }
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return json_encode("Successfully Deleted!");
    }
    public function login(Request $request){   
        $input =$request->all();
        $userdata=$request->only('email','password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($userdata)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password',
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }
        $user=auth::user();
        $session_user=Session::put('user', $user);
        return view('User.home_page')->with('user',$session_user);
    }
    public function logout(){
        Session::flush();  
        return redirect('/');
    }
}
