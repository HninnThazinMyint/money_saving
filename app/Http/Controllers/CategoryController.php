<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
       $category = new Category();
       $category=$category->paginate(2);
       $response = [
            'pagination' => [
                'total' =>  $category->total(),
                'per_page' =>  $category->perPage(),
                'current_page' =>  $category->currentPage(),
                'last_page' =>  $category->lastPage(),
                'from' =>  $category->firstItem(),
                'to' =>  $category->lastItem()
            ],
             'data' =>  $category
        ];
        return response()->json($response);      
    }
    public function getAll_Cat(){
        $category = new Category();
        $category = $category->all();
        return response()->json($category);
    }
    public function store(Request $request)
    {   
        $request->validate([
            'name'=> 'required',
            'description'=> 'required'
         ]);
        $category=new Category([
                        'category_name' =>$request->get('name'),
                        'description'   => $request->get('description')
                    ]);
        $result=$category->save();
        echo json_encode($result);
    }
    public function edit($id){
        $category = Category::find($id);
        return response()->json($category);
    }
    public function update(Request $request, $id)
    {
        $post=[
                'category_name' =>$request->get('name'),
                'description'   => $request->get('description')
        ];
        $output=Category::where('id',$id)->update($post);
        echo json_encode($output);
    }
    public function destroy($id)
    {
        $category = Category::find($id);
        $result=$category->delete();
        echo json_encode($result);
    }
}
