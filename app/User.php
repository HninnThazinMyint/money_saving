<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{	
	use \Illuminate\Auth\Authenticatable;
    protected $fillable=['name','phone','email','password','address','role'];
    protected $table = 'users';

}
