@extends('layout.master')
@section('title', 'Page Title')
@section('content')
<style type="text/css">
    .modal-body {
    height: 128px;
    margin-left: 20px;
    margin-right: 46px;
    }
    .container{
    margin-left: 14%;
    margin-top: 38px;
    }
</style>
<h1 align="center">Category Page</h1>
<button class="btn btn-success" data-toggle="modal" data-target="#myModal" id="add_btn">Add<img src="{{ asset('images/add.jpg') }}" style="width:20px;height: 20px;margin-left: 10px">
</button>
<br><br>
<!-- modal box -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Category</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="cat_id">
                <div id="row">
                    <div class="col-md-6">
                        <label>Name</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="category_name" class="form-control">
                        <span id="name_err"></span>
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Description</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <textarea id="category_description" class="form-control"></textarea>
                        <span id="description_err"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="add_category">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- ************ -->
<table class="table table-hover">
    <tr>
        <th>Category Name</th>
        <th>Description</th>
        <th colspan="2">Action</th>
    </tr>
    <tbody id="category_body"></tbody>
</table>
<nav>
    <ul class="pagination">
        <li id="pagi_li">                
        </li>
    </ul>
</nav>
<script src="{{ asset('js/app/category.js') }}"></script>
@stop