@extends('layout.master')

@section('title', 'Page Title')

@section('content')
   <h1 align="center"> Report Page</h1>
   <table class="table table-hover">
   	<tr>
   		<th>Name</th>
   		<th>Email Address</th>
   		<th>Current Money</th>
   		<th>In Money</th>
   		<th>Out Money</th>
   	</tr>
   	<tr>
   		<td>Hninn</td>
   		<td>hninnthazinmyint820@gmail.com</td>
   		<td>1000000</td>
   		<td>300000</td>
   		<td>400000</td>
   	</tr>
   	<tr>
   		<td>Mg Mg</td>
   		<td>mgmg@gmail.com</td>
   		<td>1000000</td>
   		<td>300000</td>
   		<td>400000</td>
   	</tr>
   	<tr>
   		<td>MaMa</td>
   		<td>mama@gmail.com</td>
   		<td>1000000</td>
   		<td>300000</td>
   		<td>400000</td>
   	</tr>
   </table>
   
@stop
