

<html>
    <head>
        <title>Money Saving Management System</title>
         <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
       
        <!-- <script src="{{ asset('js/jquery.js') }}"></script> -->

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>         
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/select2.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <script src="{{ asset('js/app/config.js') }}"></script>
    </head>
    <style type="text/css">
        .navbar-global {
        background-color: #3c763d;
    }

.navbar-global .navbar-brand {
  color: white;
}

.navbar-global .navbar-user > li > a
{
  color: white;
}

.navbar-primary {
  background-color: #333;
  bottom: 0px;
  left: 0px;
  position: absolute;
  top: 51px;
  width: 200px;
  z-index: 8;
  overflow: hidden;
  -webkit-transition: all 0.1s ease-in-out;
  -moz-transition: all 0.1s ease-in-out;
  transition: all 0.1s ease-in-out;
}

.navbar-primary.collapsed {
  width: 60px;
}

.navbar-primary.collapsed .glyphicon {
  font-size: 22px;
}

.navbar-primary.collapsed .nav-label {
  display: none;
}

.btn-expand-collapse {
    position: absolute;
    display: block;
    left: 0px;
    bottom:0;
    width: 100%;
    padding: 8px 0;
    border-top:solid 1px #666;
    color: grey;
    font-size: 20px;
    text-align: center;
}

.btn-expand-collapse:hover,
.btn-expand-collapse:focus {
    background-color: #222;
    color: white;
}

.btn-expand-collapse:active {
    background-color: #111;
}

.navbar-primary-menu,
.navbar-primary-menu li {
  margin:0; padding:0;
  list-style: none;
}

.navbar-primary-menu li a {
  display: block;
  padding: 10px 18px;
  text-align: left;
  border-bottom:solid 1px #444;
  color: #ccc;
}

.navbar-primary-menu li a:hover {
  background-color: #000;
  text-decoration: none;
  color: white;
}

.navbar-primary-menu li a .glyphicon {
  margin-right: 6px;
}

.navbar-primary-menu li a:hover .glyphicon {
  color: orchid;
}

.main-content {
  margin-top: 60px;
  margin-left: 200px;
  padding: 20px;
}

.collapsed + .main-content {
  margin-left: 60px;
}
    </style>
    <body>
        <div class="cc">
            <nav class="navbar navbar-inverse navbar-global navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                      <a class="navbar-brand" href="#">Money Saving System(GOSS)</a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-user navbar-right">                
                            <li id="user_name"><a href="#">
                            @if(Session::get('user') != '') 
                            {{ Session::get('user')->name }}</a></li>
                            <li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
                            @endif
                      </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>            
            <nav class="navbar-primary">
                  <a href="#" class="btn-expand-collapse"><span class="glyphicon glyphicon-envelope"></span></a> 
                @if(Session::get('user'))               
                    <ul class="navbar-primary-menu" id="menu">
                        <li>
                        @if(Session::get('user')->role==1))
                          <a href="{{ url('/user') }}"><span class="glyphicon glyphicon-user"></span><span class="nav-label">User </span></a>                        
                         @endif  
                          <a href="{{ url('/category') }}"><i class="fa fa-user"></i><span class="nav-label">Category</span></a>
                          <a href="{{ url('/transition') }}"><span class="glyphicon glyphicon-cog"></span><span class="nav-label">Transition</span></a>
                        </li>
                    </ul>
                @endif                  
            </nav>              
        </div>
        <div class="container">            
             @yield('content')  
        </div>      
    </body>
</html>
<script type="text/javascript">    
    var percentage='14%';
      $('.btn-expand-collapse').click(function(e) {
        if(percentage=='5%'){
            percentage='14%';
        }
        else{
            percentage='5%';
        }
      $('.navbar-primary').toggleClass('collapsed');
      $('.container').css('margin-left',percentage);
});
</script>