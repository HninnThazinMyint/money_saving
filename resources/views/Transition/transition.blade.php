@extends('layout.master')
@section('title', 'Page Title')
@section('content')
<?php session_start(); ?>
<style type="text/css">
    .modal-body {
    height: 328px;
    margin-left: 20px;
    margin-right: 46px;
    }
    #add_btn{
    margin-left: 63%;
    }
    .container{
    width: 1137px;
    margin-left: 14%;
    margin-top: 41px;
    }
</style>
<h1 align="center">Transition Page</h1>
<div id="row">
    <!-- <select class="js-example-basic-single auto">
        <option value="">Search User</option>     
        </select> -->
    <button class="btn btn-success" data-toggle="modal" data-target="#myModal" id="add_btn">Add<img src="{{ asset('images/add.jpg') }}" style="width:20px;height: 20px;margin-left: 10px">
    </button>
</div>
<br><br>   
<!-- modal box -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Transition</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="transition_id">
                <input type="hidden" id="add_user_id" value=" {{ Session::get('user')->id }}"> 
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Amount</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <input type="number" id="amount" class="form-control">
                        <span id="amount_err"></span>
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Status</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <select id="status" class="form-control">
                            <option value="0">In</option>
                            <option value="1">Out</option>
                        </select>
                        <span id="status_err"></span>
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Category</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <select id="category_id" class="form-control">
                            <option value="">Please Select Category</option>
                        </select>
                        <span id="category_err"></span>
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Reason</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <textarea id="reason" class="form-control"></textarea>
                        <span id="reason_err"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="add_transition">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- ************ -->
<table class="table table-hover">
    <tr>
        <th>Name</th>
        <th>Category</th>
        <th>In Money</th>
        <th>Out Money</th>
        <th>Current Money</th>
        <th>Reason</th>
        <th>Date<a href="#" order='asc' id="link"><img src="../images/up.png" style="height: 19px;width:20px;margin-top: -11px;"></a><a href="#" order='desc' id="link1"><img src="../images/down.png" style="height: 19px;width:20px;margin-top: 13px;margin-left: -20px;"></a></th>
        <th colspan="2">Action</th>
    </tr>
    <tbody id="transition_body">
    </tbody>
</table>
<nav>
    <ul class="pagination">
        <li id="pagi_li">                
        </li>
    </ul>
</nav>
<select id="pagi_no">
    <option>2</option>
    <option>4</option>
    <option>8</option>
    <option>16</option>
</select>
<script src="{{ asset('js/app/category.js') }}"></script>
<script src="{{ asset('js/app/transition.js') }}"></script>
@stop