@extends('layout.master')
@section('title', 'Page Title')
@section('content')
<style type="text/css">
    .modal-body {
    height: 350px;
    margin-left: 20px;
    margin-right: 46px;
    }
    .pagination {
    display: inline-block;
    }
    .pagination a {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
    }
    .pagination a.active {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
    }
    .pagination a:hover:not(.active) {background-color: #ddd;}
    .container{
    margin-top: 39px;
    margin-left: 14%;
    }
</style>
<?php session_start(); ?>
<h1 align="center">Admin Page</h1>
<button class="btn btn-success" data-toggle="modal" data-target="#myModal">Create User<img src="{{ asset('images/add.jpg') }}" style="width:20px;height: 20px;margin-left: 10px">
</button>
<br><br>
<!-- modal box -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create User</h4>
            </div>
            <div class="modal-body">
                <div id="row">
                    <div class="col-md-6">
                        <label>Name</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="name" class="form-control">
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Email</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <input type="Email" id="email" class="form-control">
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Password</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <input type="password" id="password" class="form-control">
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Phone</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <input type="number" id="phone" class="form-control">
                    </div>
                </div>
                <div id="row">
                    <div class="col-md-6" style="margin-top: 15px;">
                        <label>Address</label>
                    </div>
                    <div class="col-md-6" style="margin-top: 15px;">
                        <textarea class="form-control" id="address"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" id="add_user">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- ************ -->
<table class="table table-hover">
    <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Email</th>
    </tr>
    <tbody id="show_output">
    </tbody>
</table>
<nav>
    <ul class="pagination">
        <li id="pagi_li">                
        </li>
    </ul>
</nav>
<script src="{{ asset('js/app/user.js') }}"></script>
@stop