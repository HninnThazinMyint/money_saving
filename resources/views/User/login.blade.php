@extends('layout.master')


@section('content')
<style type="text/css">
	.row_put{
		margin-left: 30px;
		margin-right: 30px;
	}
	.login_img{
		margin-right: 20px;
		position: absolute;
		width: 80%;
		height: 80%;
		margin-top: 28px;
	}
	.logo{
		margin-left: 42%;
    	width: 100px;
    	height: 100px;
	}
	.row_input{
		margin-top: 1%;
    	width: 28%;
    	height: 52px;
    	margin-left: 33%;
	}
	.logo_header{
		color: #e3e8ef;
		padding-top: 7%;
	}
	.btn_login{
		margin-left: 43%;
	}
</style>
<?php session_start(); ?>
<div id="login_view">
	<!-- <img src="../images/login_bg.png" class="login_img"> -->
	<div style="position: relative;">
		<h1 align="center" class="logo_header" style="color: green;
    margin-left: 9%;">Welcome from Money Saving System</h1>
    	{{ isset($_SESSION["user_name"])? $_SESSION["user_name"] :'delete' }}
		<img src="../images/user.png" class="logo"> 
    <form action="login_user" method="post">   
		  <div id="row" class="row_input">      
      <input type="hidden" name="_token"  id="ctr_token" value="<?php echo csrf_token() ?>">
			<input type="email" id="email" class="form-control" placeholder="Username" name="email">
		</div>
		<div id="row" class="row_input">
			<input type="password" class="form-control" id="password" placeholder="Password" name="password">
		</div>	
		<div id="row" class="btn_login">
			<input type="submit" value="Login" class="btn btn-success">
		</div>
    </form>
	</div>
</div>
<!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#user_view").hide();
	});
	$("#login_btn").click(function(){
		var username=$("#email").val();
		var password=$("#password").val();
		$.ajax({
    		type: 'post',
    		url : "login_user",
    		dataType : 'json',
    		data : {
    			email : username,
    			password : password,
          _token : $('#ctr_token').val() 
    		},
    		success: function(e){
          if(e.token!=''){
            window.location='/home';
          }    			
    		},
    		error : function(error){
          console.log(error);    			
    		}
    	});
	});
	
</script> -->
@stop